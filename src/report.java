import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.BorderLayout;
import java.net.PasswordAuthentication;
import java.util.Objects;
import javax.swing.JFrame;
import javax.swing.JPasswordField;

public class report {
    private JPanel root;
    private JLabel topLabel;
    private JLabel orderedItemsLabel;
    private JLabel totalText;
    private JButton menu1Button;
    private JButton menu2Button;
    private JButton menu3Button;
    private JButton menu4Button;
    private JButton menu5Button;
    private JButton menu6Button;
    private JButton CheckoutButton;
    private JTextPane orderTextPane;
    private JButton languageButton;
    private JButton optionButton;
    private JButton japanese;
    private JButton english;
    private JButton korean;
    private JButton chinese;
    private JButton spanish;
    private JButton french;
    private JFrame frame; //言語を変更するときに開く
    private JFrame input_pass; //パスワード入力画面用
    private JFrame option; //option用
    private JPasswordField pass;
    private JTextField password;
    private JButton check_passButton;
    private JButton delete_passButton;
    private JButton num_0Button;
    private JButton num_1Button;
    private JButton num_2Button;
    private JButton num_3Button;
    private JButton num_4Button;
    private JButton num_5Button;
    private JButton num_6Button;
    private JButton num_7Button;
    private JButton num_8Button;
    private JButton num_9Button;
    private JPanel pass_upPanel;
    private JPanel pass_downPanel;
    private JLabel passLabel;
    private JButton enterButton;

    private JPanel language;
    int sum=0;

    static String orderConfirm(String food){
        String s = "Would you like to order " + food + "?";
        return s;
    }
    static String message(String food,int cost){
        String s = food + " " + cost + "yen";
        return s;
    }
    static String message_dialog(String food){
        String s = "Thank you for ordering " + food + "! It will be served as soon as possible";
        return s;
    }

    //言語変更用
    void change_Language(JFrame frame,JButton btn,String str){
        btn = new JButton(str);
        frame.add(btn);
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,str+"?",
                        "Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(confirmation==0){
                    frame.dispose();
                }
            }
        });
    }

    /*パスワード入力
    txtに0~9の数字を追加
    */
    void enter_pass(JTextField txt,JButton btn){
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String str = txt.getText();
                str += btn.getText();
                txt.setText(str);
            }
        });
    }


    void order(String food,int cost){
        int confirmation = JOptionPane.showConfirmDialog(null,orderConfirm(food),
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation==0){
            sum+=cost;
            String currentText = orderTextPane.getText();
            orderTextPane.setText(currentText + message(food,cost) + "\n");
            JOptionPane.showMessageDialog(null,message_dialog(food));
            totalText.setText("Total " + (sum) + "yen");
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new report().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public report() {
        menu1Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",750);
            }
        });
        menu1Button.setIcon(new ImageIcon(
                Objects.requireNonNull(this.getClass().getResource("menu1.jpeg"))
        ));


        menu2Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Humberger",780);

            }
        });
        menu2Button.setIcon(new ImageIcon(
                this.getClass().getResource("menu2.jpeg")
        ));

        menu3Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Lib",1200);
            }
        });
        menu3Button.setIcon(new ImageIcon(
                this.getClass().getResource("menu3.jpeg")
        ));

        menu4Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Fresh seafood rice bowl",800);
            }
        });
        menu4Button.setIcon(new ImageIcon(
                this.getClass().getResource("menu4.jpeg")
        ));

        menu5Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sweets",650);

            }
        });
        menu5Button.setIcon(new ImageIcon(
                this.getClass().getResource("menu5.jpeg")
        ));

        menu6Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Coffee",420);

            }
        });
        menu6Button.setIcon(new ImageIcon(
                this.getClass().getResource("menu6.jpeg")
        ));


        CheckoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int Checkout_confirmation = JOptionPane.showConfirmDialog(null,"Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(Checkout_confirmation==0){
                    JOptionPane.showMessageDialog(null,"Thank you. The total price is " + sum + " yen");
                    orderTextPane.setText("");
                    totalText.setText("Total 0 yen");
                    sum=0;
                }
            }
        });


        languageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int Checkout_confirmation = JOptionPane.showConfirmDialog(null,"Do you use another language??",
                        "Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(Checkout_confirmation==0){
                    frame = new JFrame("Language");
                    frame.setSize(600, 400);
                    frame.setLayout(new GridLayout(2,3));
                    frame.setVisible(true);
                    change_Language(frame,japanese,"Japanese");
                    change_Language(frame,english,"English");
                    change_Language(frame,korean,"Korean");
                    change_Language(frame,chinese,"Chinese");
                    change_Language(frame,spanish,"Spanish");
                    change_Language(frame,french,"French");
                }
            }
        });
        optionButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                option = new JFrame("option");
                option.setSize(600,400);

                input_pass = new JFrame("Option");
                input_pass.setSize(300, 400);
                input_pass.setLayout(new GridLayout(2,1)); //frameを上下の二つに分割
                input_pass.setVisible(true);

                passLabel = new JLabel("Enter password");

                pass = new JPasswordField(4);

                password = new JTextField(4);

                enterButton = new JButton("Check");

                pass_upPanel = new JPanel();
                pass_downPanel = new JPanel();

                input_pass.add(pass_upPanel); //分割したframeの上枠にパスワード入力欄を配置するためのpanelを追加
                input_pass.add(pass_downPanel); //分割したframeの下枠に数字ボタンを配置するためのpanelを追加

                pass_upPanel.add(password,BorderLayout.CENTER);

                check_passButton = new JButton("check");
                delete_passButton = new JButton("delete");

                num_0Button = new JButton("0");
                num_1Button = new JButton("1");
                num_2Button = new JButton("2");
                num_3Button = new JButton("3");
                num_4Button = new JButton("4");
                num_5Button = new JButton("5");
                num_6Button = new JButton("6");
                num_7Button = new JButton("7");
                num_8Button = new JButton("8");
                num_9Button = new JButton("9");

                //数字ボタン、deleteボタン、checkボタンを追加
                pass_downPanel.add(num_1Button);
                pass_downPanel.add(num_2Button);
                pass_downPanel.add(num_3Button);
                pass_downPanel.add(num_4Button);
                pass_downPanel.add(num_5Button);
                pass_downPanel.add(num_6Button);
                pass_downPanel.add(num_7Button);
                pass_downPanel.add(num_8Button);
                pass_downPanel.add(num_9Button);
                pass_downPanel.add(delete_passButton);
                pass_downPanel.add(num_0Button);
                pass_downPanel.add(check_passButton);

                enter_pass(password,num_0Button);
                enter_pass(password,num_1Button);
                enter_pass(password,num_2Button);
                enter_pass(password,num_3Button);
                enter_pass(password,num_4Button);
                enter_pass(password,num_5Button);
                enter_pass(password,num_6Button);
                enter_pass(password,num_7Button);
                enter_pass(password,num_8Button);
                enter_pass(password,num_9Button);

                //deleteボタン
                delete_passButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String str = password.getText();
                        str="";
                        password.setText(str);
                    }
                });

                //checkボタン
                check_passButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        String str = password.getText();
                        if(str.equals("1111")) {
                            JOptionPane.showMessageDialog(null, "OK");
                            input_pass.dispose();
                            option.setVisible(true); //パスワードが合っていたらoption画面を可視化
                        }
                        else{
                            JOptionPane.showMessageDialog(null, "password is incorrect");
                            password.setText("");
                        }

                    }
                });



            }
        });
    }

}
