import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class report_original {
    private JPanel root;
    private JLabel topLabel;
    private JLabel orderedItemsLabel;
    private JLabel totalText;
    private JButton menu1Button;
    private JButton menu2Button;
    private JButton menu3Button;
    private JButton menu4Button;
    private JButton menu5Button;
    private JButton menu6Button;
    private JButton CheckoutButton;
    private JTextPane orderTextPane;
    int sum=0;

    static String orderConfirm(String food){
        String s = "Would you like to order " + food + "?";
        return s;
    }
    static String message(String food,int cost){
        String s = food + " " + cost + "yen";
        return s;
    }
    static String message_dialog(String food){
        String s = "Thank you for ordering " + food + "! It will be served as soon as possible";
        return s;
    }


    void order(String food,int cost){
        int confirmation = JOptionPane.showConfirmDialog(null,orderConfirm(food),
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation==0){
            sum+=cost;
            String currentText = orderTextPane.getText();
            orderTextPane.setText(currentText + message(food,cost) + "\n");
            JOptionPane.showMessageDialog(null,message_dialog(food));
            totalText.setText("Total " + (sum) + "yen");
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("SimpleFoodOrderingMachine");
        frame.setContentPane(new report_original().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

    public report_original() {
        menu1Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",750);
            }
        });
        menu1Button.setIcon(new ImageIcon(
                this.getClass().getResource("menu1.jpeg")
        ));

        menu2Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Humberger",780);

            }
        });
        menu2Button.setIcon(new ImageIcon(
                this.getClass().getResource("menu2.jpeg")
        ));

        menu3Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Lib",1200);
            }
        });
        menu3Button.setIcon(new ImageIcon(
                this.getClass().getResource("menu3.jpeg")
        ));

        menu4Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Fresh seafood rice bowl",800);
            }
        });
        menu4Button.setIcon(new ImageIcon(
                this.getClass().getResource("menu4.jpeg")
        ));

        menu5Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sweets",650);

            }
        });
        menu5Button.setIcon(new ImageIcon(
                this.getClass().getResource("menu5.jpeg")
        ));

        menu6Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Coffee",420);

            }
        });
        menu6Button.setIcon(new ImageIcon(
                this.getClass().getResource("menu6.jpeg")
        ));

        CheckoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int Checkout_confirmation = JOptionPane.showConfirmDialog(null,"Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);
                if(Checkout_confirmation==0){
                    JOptionPane.showMessageDialog(null,"Thank you. The total price is " + sum + " yen");
                    orderTextPane.setText("");
                    totalText.setText("");
                    sum=0;
                }
            }
        });
    }

}
